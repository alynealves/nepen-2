import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Nunito"),
      title: "NEPEN BLE",
      home: Scaffold(
        body: Container(
          color: Colors.deepPurple[800],
          child: AppContainer(),
        ),
      ),
    );
  }
}

class AppContainer extends StatefulWidget {
  @override
  _AppContainerState createState() => _AppContainerState();
}

class _AppContainerState extends State<AppContainer> {
  final List<String> menuItems = ["Home", "Buscar Dispositivos", "Logs de Comunicação", "Configurar a NIC"];
  final List<String> menuIcons = ["icon_home", "icon_bluetooth_searching", "icon_chart", "icon_settings"];

  bool sidebarOpen = false;

  double yOffset = 0;
  double xOffset = 60;
  double pageScale = 1;

  int selectedMenuItem = 0;

  String pageTitle = "Homepage";

  void setSidebarState() {
    setState(() {
      xOffset = sidebarOpen ? 265 : 60;
      yOffset = sidebarOpen ? 70 : 0;
      pageScale = sidebarOpen ? 0.9 : 1;
    });
  }

  void setPageTitle() {
    switch(selectedMenuItem) {
      case 0:
        pageTitle = "Home";
        break;
      case 1:
        pageTitle = "Dispositivos BLE";
        break;
      case 2:
        pageTitle = "Logs de Comunicação";
        break;
      case 3:
        pageTitle = "Configurar a NIC";
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 24),
                  child: Container(
                    color: Colors.deepPurple[600],
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            sidebarOpen = true;
                            setSidebarState();
                          },
                          child: Container(
                              padding: const EdgeInsets.all(20),
                              child: Image.asset("assets/images/icon_search.png")
                          ),
                        ),
                        Container(
                            child: Expanded(
                              child: TextField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: const EdgeInsets.all(20),
                                    hintText: "Buscar por...",
                                    hintStyle: TextStyle(
                                      color: Colors.white,
                                    )
                                ),
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            )
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Expanded(
                      child: new ListView.builder(
                          itemCount: menuItems.length,
                          itemBuilder: (context, index) => GestureDetector(
                            onTap: () {
                              sidebarOpen = false;
                              selectedMenuItem = index;
                              setSidebarState();
                              setPageTitle();
                            },
                            child: MenuItem(
                              itemIcon: menuIcons[index],
                              itemText: menuItems[index],
                              selected: selectedMenuItem,
                              position: index,
                            ),
                          )
                      )
                  ),
                ),

              ],
            ),
          ),
          AnimatedContainer(
            curve: Curves.easeInOut,
            duration: Duration(milliseconds: 200),
            transform: Matrix4.translationValues(xOffset, yOffset, 1.0)..scale(pageScale),
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: sidebarOpen ? BorderRadius.circular(20) : BorderRadius.circular(0)
            ),
            child: Column(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(top: 24),
                    height: 60,
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            sidebarOpen = !sidebarOpen;
                            setSidebarState();
                          },
                          child: Container(
                              color: Colors.white,
                              padding: const EdgeInsets.all(20),
                              child: Icon(Icons.menu)
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20, bottom: 20),
                          child: Text(
                            pageTitle,
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        )
                      ],
                    )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class MenuItem extends StatelessWidget {
  final String itemText;
  final String itemIcon;
  final int selected;
  final int position;
  MenuItem({this.itemText, this.itemIcon, this.selected, this.position});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: selected == position ? Colors.deepPurple[700] : Colors.deepPurple[800],
      child: Row(
        children: <Widget>[
          Container(
              padding: const EdgeInsets.all(20),
              child: Image.asset("assets/images/$itemIcon.png")
          ),
          Container(
            padding: const EdgeInsets.all(20),
            child: Text(
              itemText,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16
              ),
            ),
          )
        ],
      ),
    );
  }
}


